package conversiones.pico.jennfier.facci.cuartoa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class ActividadPasarParametro extends AppCompatActivity {

    EditText cajaDatos;
    Button enviar2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_pasar_parametro);

        cajaDatos = (EditText) findViewById(R.id.editText6);
        enviar2 = (Button)findViewById(R.id.button8);
        enviar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 Intent intent = new Intent(ActividadPasarParametro.this, ActividadRecibirParametro.class);
                 Bundle bundle = new Bundle();
                 bundle.putString("dato", cajaDatos.getText().toString());
                 intent.putExtras(bundle);
                 startActivity(intent);
                 }
        });
    }
}
