package conversiones.pico.jennfier.facci.cuartoa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ActividadPrincipal extends AppCompatActivity {

    Button buscar;
    Button login;
    Button registrar;
    Button pasarparametro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_principal);

        buscar = (Button)findViewById(R.id.button3);

        buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent buscar =new Intent(ActividadPrincipal.this, ActividadBuscar.class);
                startActivity(buscar);
            }
        });

        login = (Button)findViewById(R.id.button);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent login =new Intent(ActividadPrincipal.this, ActividadLogin.class);
                startActivity(login);
            }
        });

        registrar = (Button)findViewById(R.id.button2);

        registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registrar =new Intent(ActividadPrincipal.this, ActividadRegistrar.class);
                startActivity(registrar);
            }
        });
        pasarparametro = (Button)findViewById(R.id.button7);

        pasarparametro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pasarparametro =new Intent(ActividadPrincipal.this, ActividadPasarParametro.class);
                startActivity(pasarparametro);
            }
        });


    }


}

